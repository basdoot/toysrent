$(document).ready(() => {
  $(document).on('show.bs.modal', '#delete-prompt', (event) => {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var name = button.data('name')
    var url = button.data('url')
    var modal = $(event.currentTarget)
    modal.find('.modal-title').text('Delete ' + name + '?')
    modal.find('.modal-text').text('Are you sure you want to delete ' + name + '?')
    modal.find('.delete-link').attr('href', url + id)
  })
  $(document).on('click', '.add-address', (event) => {
    event.preventDefault();
    $('.multi-address').append(`
    <div class="form-row mt-3">
      <div class="col-sm-3 input-group">
        <input type="text" class="form-control" placeholder="Name" aria-label="Name" name="address_name[]" required> <!-- use [] for multi fields -->  
      </div>
      <div class="col input-group">
        <input type="text" class="form-control" placeholder="Address" aria-label="Address" name="address[]" required> <!-- use [] for multi fields -->  
      </div>
      <div class="col-sm-1">
        <button class="btn btn-outline-secondary remove-address" style="width: 100%">Delete</button>
      </div>
    </div>`);
  })
  $(document).on('click', '.remove-address', (event) => {
    event.preventDefault();
    $(event.currentTarget).parent().parent().remove();
  })
})