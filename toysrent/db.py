import os
import psycopg2 as pg


class database(object):
    __instance = None

    @staticmethod
    def db():
        if database.__instance is None:
            database()
        return database.__instance

    def __init__(self):
        if database.__instance is not None:
            raise Exception("Only one Database instance can be running!")
        else:
            database.__instance = self
            try:
                dbUrl = os.environ['DATABASE_URL'][11:]
            except:
                dbUrl = 'toysrent:toysrent@ta-sc.ddns.net:5432/toysrent'
            dbName = dbUrl.split('/')[1]
            dbUser = dbUrl.split('@')[0].split(':')[0]
            dbPass = dbUrl.split('@')[0].split(':')[1]
            dbHost = dbUrl.split('@')[1].split(':')[0]
            self.__access = pg.connect(host=dbHost, database=dbName, user=dbUser, password=dbPass)
            print('DB - OK  :: Connected to {}!'.format(dbHost))

    # execute SQL query
    def query(self, sql, *arg):
        print('DB - SQL :: ', sql.format(arg))          # DEBUG SQL STATEMENT
        cur = self.__access.cursor()
        cur.execute(sql, arg)
        try:
            result = cur.fetchall()
        except Exception:
            result = None
        self.__access.commit()
        return result

    # simplified execution for Stored Procedures
    def exec(self, proc, *arg):
        print('DB - SP  :: ', proc + str(arg))    # DEBUG PROC STATEMENT
        cur = self.__access.cursor()
        try:
            cur.callproc(proc, arg)
            result = cur.fetchall()
        except Exception:
            result = None
        self.__access.commit()
        return result