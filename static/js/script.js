$(document).ready(() => {
  $(document).on('show.bs.modal', '#delete-prompt', (event) => {
    let button = $(event.relatedTarget) // Button that triggered the modal
    let id = button.data('id') // Extract info from data-* attributes
    let name = button.data('name')
    let url = button.data('url')
    let modal = $(event.currentTarget)
    modal.find('.modal-title').text('Delete ' + name + '?')
    modal.find('.modal-text').text('Are you sure you want to delete ' + name + '?')
    modal.find('.delete-link').attr('href', url + id)
  })
  $(document).on('click', '.add-address', (event) => {
    event.preventDefault();
    $('.first-row').before(`
        <div class="form-row mb-3">
          <div class="col-sm-2 input-group">
            <input type="text" class="form-control" placeholder="Name" aria-label="Name" name="address_name[]" required> <!-- use [] for multi fields -->  
          </div>
          <div class="col-sm-3 input-group">
            <input type="text" class="form-control" placeholder="Road" aria-label="Road" name="address_road[]" required> <!-- use [] for multi fields -->  
          </div>
          <div class="col-sm-2 input-group">
            <input type="text" class="form-control" placeholder="Number" aria-label="Number" name="address_number[]" required> <!-- use [] for multi fields -->  
          </div>
          <div class="col-sm-2 input-group">
            <input type="text" class="form-control" placeholder="City" aria-label="City" name="address_city[]" required> <!-- use [] for multi fields -->  
          </div>
          <div class="col-sm-2 input-group">
            <input type="number" class="form-control" placeholder="Postcode" aria-label="Postcode" name="address_postcode[]" required> <!-- use [] for multi fields -->  
          </div>
          <div class="col-sm-1">
            <button class="btn btn-outline-secondary remove-address" style="width: 100%">Delete</button>
          </div>
        </div>`);
  })
  $(document).on('click', '.remove-address', (event) => {
    event.preventDefault();
    $(event.currentTarget).parent().parent().remove();
  })

  sortMethod = getParam('sort');
  filterMethod = getParam('filter');
  pageNumber = getParam('page');
  $("#sort-field").change((event) => {
    sortMethod = $(event.currentTarget).children("option:selected").val();
    refresh();
  });
  
  $("#filter-field").change((event) => {
    filterMethod = $(event.currentTarget).children("option:selected").val();
    refresh();
  });
  
  $(".page-link").click((event) => {
    pageNumber = $(event.currentTarget).text();
    refresh();
  });

  function refresh() {
    window.location = location.protocol + '//' + location.host + location.pathname + '?' + $.param({page: pageNumber, sort: sortMethod, filter: filterMethod})
  }

  function getParam(key) {
    let url = new URL(window.location.href);
    return url.searchParams.get(key);
  }
})