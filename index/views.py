from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from toysrent.db import database

page_size = 20


def index(request):
    # EXAMPLE PROCEDURE CALL
    db = database.db()
    res = db.exec('get_goods_detail', ('16'))
    print(res)

    response = {
        'page': 'index'
    }
    return render(request, 'index.html', response)


# ------------------- USER

@csrf_exempt
def login(request):
    if request.method == 'GET':
        response = {
            'page': 'login'
        }
        return render(request, 'login.html', response)
    else:
        # TODO attempt to verify login
        # ATM, by default its assumed to be an admin
        request.session['admin'] = True
        request.session['id_number'] = 9001543
        return redirect('order-list')


@csrf_exempt
def register(request):
    if request.method == 'GET':
        type = request.GET.get('type')
        if (type == 'admin'):
            response = {
                'page': 'register',
            }
            return render(request, 'register-admin.html', response)
        else:
            response = {
                'page': 'register',
            }
            return render(request, 'register-member.html', response)
    else:
        type = request.GET.get('type')
        if (type == 'admin'):
            # TODO create admin user
            request.session['admin'] = True
            request.session['id_number'] = 9001543
            return redirect('order-list')
        else:
            # TODO create member user
            request.session['admin'] = False
            request.session['id_number'] = 9001543
            return redirect('goods-list')


def logout(request):
    del request.session['admin']
    del request.session['id_number']
    return redirect('index')


def profile(request):
    user_info = database.db().query('SELECT * FROM APP_USER NATURAL JOIN MEMBER WHERE id_number=\'{}\''.format(request.session['id_number']))
    user_address = database.db().query('SELECT * FROM ADDRESS WHERE member_id_num=\'{}\''.format(request.session['id_number']))
    response = {
        'admin': request.session['admin'],
        'page': 'profile',
        'info': user_info,
        'addresses': user_address
    }
    print(user_address)
    return render(request, 'profile.html', response)


# ------------------- CHATS


def chat_list(request):  # TODO FOR ADMIN

    response = {
        'admin': request.session['admin'],
        'page': 'chat'
    }
    chats = database.db().query('SELECT * FROM CHAT')

    response = {
        'chats': chats
    }

    return render(request, 'chat-list.html', response)


def chats_reply(request):
    response = {
        'admin': request.session['admin'],
        'page': 'chat',
        'update': False
    }
    return render(request, 'chat-reply.html', response)


def goods_review(request):
    response = {
        'admin': request.session['admin'],
        'page': 'chat',
        'update': False
    }
    return render(request, 'review-list.html', response)


# ------------------- GOODS

def goods_list(request):
    sort_method = 'goods_id' if request.GET.get('sort', 'goods_id') == '' else request.GET.get('sort', 'goods_id')
    filter_method = None if request.GET.get('filter', None) == '' else request.GET.get('filter', None)
    page_number = int(1 if request.GET.get('page', 1) == '' else request.GET.get('page', 1))

    categories = database.db().query('SELECT * FROM CATEGORY')

    if filter_method is not None:
        base = 'SELECT goods_id, item_name, color, condition FROM GOODS NATURAL JOIN ITEM_CATEGORY WHERE category_name=\'{}\' ORDER BY {}'.format(
            filter_method, sort_method)
    else:
        base = 'SELECT goods_id, item_name, color, condition FROM GOODS NATURAL JOIN ITEM_CATEGORY ORDER BY {}'.format(
            sort_method)
    print(sort_method)
    print(base)
    goods = database.db().query(base)
    print(goods)

    response = {
        'admin': request.session['admin'],
        'page': 'goods',
        'goods': page_split(goods, page_number - 1),
        'sort': sort_method,
        'filter': filter_method,
        'categories': categories,
        'page': page_number,
        'pages': range(1, ((len(goods)-1) // page_size) + 2)
    }
    return render(request, 'goods-list.html', response)


@csrf_exempt
def goods_add(request):
    if (request.method == 'GET'):
        items = database.db().query('SELECT * FROM ITEM')
        members = database.db().query('SELECT * FROM APP_USER U, MEMBER M WHERE U.ID_NUMBER=M.ID_NUMBER')
        levels = database.db().query('SELECT * FROM MEMBERSHIP_LEVEL')
        response = {
            'admin': request.session['admin'],
            'page': 'goods',
            'update': False,
            'items': items,
            'members': members,
            'pricing': levels
        }
        return render(request, 'goods-add.html', response)
    else:
        goods_id = request.POST.get('id')
        goods_name = request.POST.get('name')
        goods_color = request.POST.get('color')
        goods_photo = request.POST.get('photo')
        goods_condition = request.POST.get('condition')
        goods_usage = int(request.POST.get('usage'))
        goods_owner = request.POST.get('owner')
        goods_percentage = request.POST.getlist('percentage[]')
        goods_price = request.POST.getlist('price[]')
        database.db().query('INSERT INTO goods VALUES(\'{}\', \'{}\', \'{}\', \'{}\', \'{}\', {}, \'{}\')'.format(goods_id, goods_name, \
                           goods_color, goods_photo, goods_condition, \
                           goods_usage, goods_owner))
        levels = database.db().query('SELECT * FROM MEMBERSHIP_LEVEL')
        for i, level in enumerate(levels):
            database.db().exec(
                'INSERT INTO ITEM_LEVEL_INFO VALUES (%s, %s, %s, %s) ON CONFLICT (GOODS_ID, LEVEL_NAME) DO UPDATE SET RENT_PRICE=%s, ROYALTY_PORTION=%s',
                goods_id, level[0], goods_price[i], goods_percentage[i], goods_price[i], goods_percentage[i])
        return redirect('goods-list')


@csrf_exempt
def goods_update(request):
    if (request.method == 'GET'):
        goods_id = request.GET.get('id')
        detail = database.db().exec('get_goods_detail', goods_id)
        items = database.db().query('SELECT * FROM ITEM')
        members = database.db().query('SELECT * FROM APP_USER U, MEMBER M WHERE U.ID_NUMBER=M.ID_NUMBER')
        pricing = database.db().query('SELECT * FROM ITEM_LEVEL_INFO WHERE GOODS_ID=%s', goods_id)
        levels = database.db().query('SELECT * FROM MEMBERSHIP_LEVEL')

        merged_pricing = []
        for level in levels:
            found = False
            for price in pricing:
                if level[0] == price[1]:
                    merged_pricing.append((level[0], price[3], price[2]))
                    found = True
                    break
            if not found:
                merged_pricing.append((level[0], "", ""))
        response = {
            'admin': request.session['admin'],
            'page': 'goods',
            'update': True,
            'detail': detail,
            'items': items,
            'members': members,
            'pricing': merged_pricing
        }
        return render(request, 'goods-update.html', response)
    else:
        old_id = request.GET.get('id')
        goods_id = request.POST.get('id')
        goods_name = request.POST.get('name')
        goods_color = request.POST.get('color')
        goods_photo = request.POST.get('photo')
        goods_condition = request.POST.get('condition')
        goods_usage = int(request.POST.get('usage'))
        goods_owner = request.POST.get('owner')
        goods_percentage = request.POST.getlist('percentage[]')
        goods_price = request.POST.getlist('price[]')
        database.db().exec('update_goods', goods_id, goods_id, goods_name, \
                           goods_color, goods_photo, goods_condition, \
                           goods_usage, goods_owner)
        levels = database.db().query('SELECT * FROM MEMBERSHIP_LEVEL')
        for i, level in enumerate(levels):
            database.db().query(
                'INSERT INTO ITEM_LEVEL_INFO VALUES (%s, %s, %s, %s) ON CONFLICT (GOODS_ID, LEVEL_NAME) DO UPDATE SET RENT_PRICE=%s, ROYALTY_PORTION=%s',
                goods_id, level[0], goods_price[i], goods_percentage[i], goods_price[i], goods_percentage[i])
        return redirect('goods-list')


def goods_delete(request):
    goods_id = request.GET.get('id')
    database.db().query('DELETE FROM GOODS WHERE goods_id=%s', goods_id)
    return redirect('goods-list')


def goods_detail(request):
    goods_id = request.GET.get('id')
    detail = database.db().exec('get_goods_detail', goods_id)
    pricing = database.db().query('SELECT * FROM ITEM_LEVEL_INFO WHERE GOODS_ID=%s', goods_id)
    levels = database.db().query('SELECT * FROM MEMBERSHIP_LEVEL')
    reviews = database.db().exec("get_review",goods_id)


    merged_pricing = []
    for level in levels:
        found = False
        for price in pricing:
            if level[0] == price[1]:
                merged_pricing.append((level[0], price[3], price[2]))
                found = True
                break
        if not found:
            merged_pricing.append((level[0], "", ""))
    response = {
        'admin': request.session['admin'],
        'page': 'goods',
        'detail': detail,
        'pricing': merged_pricing,
        'reviews': reviews
    }
    return render(request, 'goods-detail.html', response)


# ------------------- ITEM

def item_list(request):
    sort_method = 'name' if request.GET.get('sort', 'name') == '' else request.GET.get('sort', 'name')
    filter_method = None if request.GET.get('filter', None) == '' else request.GET.get('filter', None)
    page_number = int(1 if request.GET.get('page', 1) == '' else request.GET.get('page', 1))

    categories = database.db().query('SELECT * FROM CATEGORY')

    if filter_method is not None:
        base = 'SELECT DISTINCT * FROM (SELECT * FROM ITEM_CATEGORY WHERE category_name=\'{}\') AS Q ORDER BY category_name'.format(
            filter_method, sort_method)
    else:
        base = 'SELECT * FROM ITEM_CATEGORY  AS Q ORDER BY item_name'.format(
            sort_method)
    item = database.db().query(base)

    response = {
        'admin': request.session['admin'],
        'page': 'item',
        'item': page_split(item, page_number - 1),
        'sort': sort_method,
        'filter': filter_method,
        'categories': categories,
        'page': page_number,
        'pages': range(1, ((len(item)-1) // page_size) + 2)
    }
    return render(request, 'item-list.html', response)


@csrf_exempt
def item_add(request):
    if request.method == 'GET':
        response = {
            'admin': request.session['admin'],
            'page': 'item',
            'update': False
        }
        return render(request, 'item-add.html', response)
    else:
        item_name = request.POST.get('name')
        item_description = request.POST.get('description')
        item_age_min = int(request.POST.get('min'))
        item_age_max = int(request.POST.get('max'))
        item_material = request.POST.get('material')
        item_category = request.POST.get('category')
        database.db().exec('INSERT INTO ITEM(name,description,min_age,max_age,material) VALUES(%s,%s,%s,%s,%s)',
                           item_name,
                           item_description, item_age_min, item_age_max, item_material)
        database.db().exec('INSERT INTO ITEM_CATEGORY(item_name,category_name) VALUES(%s,%s)', item_name,
                           item_category)
        return redirect('item-list')


@csrf_exempt
def item_update(request):
    if request.method == 'GET':
        name = request.GET.get('id')
        detail = database.db().query('SELECT * FROM ITEM  WHERE name = %s', name)

        response = {
            'admin': request.session['admin'],
            'page': 'item',
            'detail': detail,
            'update': True
        }
        return render(request, 'item-update.html', response)
    else:
        item_name = request.POST.get('name')
        item_description = request.POST.get('description')
        item_age_min = int(request.POST.get('min'))
        item_age_max = int(request.POST.get('max'))
        item_material = request.POST.get('material')
        item_category = request.POST.get('category')
        database.db().exec('update_item', item_name, item_description, item_age_min, item_age_max, item_material,
                           item_category)
        return redirect('item-list')


def item_delete(request):
    # TODO delete item
    item_name = request.GET.get('id')
    database.db().query('DELETE FROM ITEM_CATEGORY WHERE item_name=%s', item_name)
    database.db().query('DELETE FROM ITEM WHERE name=%s', item_name)
    return redirect('item-list')


# ------------------- LEVEL

def level_list(request):
    levels = database.db().query('SELECT * FROM MEMBERSHIP_LEVEL ORDER BY minimum_points ASC')
    numbered_lvl = []
    for i, level in enumerate(levels):
        numbered_lvl.append((i + 1,) + level)
    response = {
        'admin': request.session['admin'],
        'levels': numbered_lvl,
        'page': 'level'
    }
    return render(request, 'level-list.html', response)


@csrf_exempt
def level_add(request):
    if (request.method == 'GET'):
        response = {
            'admin': request.session['admin'],
            'page': 'level',
            'update': False
        }
        return render(request, 'level-add.html', response)
    else:
        level_name = request.POST.get('name')
        level_minimum = request.POST.get('minimum')
        level_description = request.POST.get('description')

        database.db().query('INSERT INTO MEMBERSHIP_LEVEL VALUES (%s, %s, %s)', \
                            level_name, level_minimum, level_description)

        return redirect('level-list')


@csrf_exempt
def level_update(request):
    if (request.method == 'GET'):
        level_name = request.GET.get('name')

        level = database.db().query('SELECT * FROM MEMBERSHIP_LEVEL WHERE level_name=%s', level_name)
        response = {
            'admin': request.session['admin'],
            'level': level,
            'page': 'level',
            'update': True
        }
        return render(request, 'level-update.html', response)
    else:
        old_name = request.GET.get('name')
        level_name = request.POST.get('name')
        level_minimum = request.POST.get('minimum')
        level_description = request.POST.get('description')

        database.db().query(
            'UPDATE MEMBERSHIP_LEVEL SET level_name=%s, minimum_points=%s, description=%s WHERE level_name=%s', \
            level_name, level_minimum, level_description, old_name)

        return redirect('level-list')


def level_delete(request):
    level_name = request.GET.get('name')
    database.db().query('DELETE FROM MEMBERSHIP_LEVEL WHERE level_name=%s', level_name)
    return redirect('level-list')


# ------------------- ORDER

def order_list(request):
    filter_method = None if request.GET.get('filter', None) == '' else request.GET.get('filter', None)
    page_number = int(1 if request.GET.get('page', 1) == '' else request.GET.get('page', 1))
    status = database.db().query('SELECT * FROM STATUS')

    if filter_method is not None:
        base = 'SELECT DISTINCT * FROM (SELECT * from app_order WHERE status=\'{}\') AS Q'.format(
            filter_method)
    else:
        base = 'SELECT DISTINCT * FROM (SELECT * FROM app_order)  AS Q'
    print(base)
    order = database.db().query(base)
    response = {
        'admin': request.session['admin'],
        'page': 'order',
        'order': page_split(order, page_number - 1),
        'filter': filter_method,
        'page': page_number,
        'pages': range(1, ((len(order)-1) // page_size) + 2),
        'status': status
    }
    return render(request, 'order-list.html', response)


@csrf_exempt
def order_add(request):
    if (request.method == 'GET'):
        member = database.db().query('SELECT * FROM member')
        goods = database.db().query('SELECT * FROM goods')
        status = database.db().query('SELECT * FROM status')
        response = {
            'admin': request.session['admin'],
            'page': 'order',
            'update': False,
            'member': member,
            'goods': goods,
            'status': status
        }
        return render(request, 'order-add.html', response)
    else:
        order_id = request.POST.get('id')
        orderer = request.POST.get('member')
        order_goods_id = request.POST.get('goods')
        order_start_date = request.POST.get('start')
        order_item_duration = int(request.POST.get('duration'))
        order_end_date = request.POST.get('end')
        order_status = request.POST.get('status')
        order_price = int(request.POST.get('price'))
        database.db().exec('create_order', order_id, 1, order_price, order_price * 4,
                           orderer, order_status)
        database.db().exec('create_ordered_goods',order_id,order_goods_id,order_item_duration,order_status)
        return redirect('delivery-list')

@csrf_exempt
def order_update(request):
    if (request.method == 'GET'):
        goods_id = request.GET.get('id')
        response = {
            'admin': request.session['admin'],
            'page': 'order',
            'update': True
        }
        return render(request, 'order-update.html', response)
    else:
        return redirect('order-list')


def order_delete(request):
    # TODO delete order
    return redirect('order-list')


# ------------------- DELIVERY

def delivery_list(request):
    # sort_method = 'name' if request.GET.get('sort', 'name') == '' else request.GET.get('sort', 'name')
    # filter_method = None if request.GET.get('filter', None) == '' else request.GET.get('filter', None)

    page_number = int(1 if request.GET.get('page', 1) == '' else request.GET.get('page', 1))
    # user_delv = database.db(),
    delivery = database.db().query('SELECT * FROM DELIVERY ORDER BY date ASC')
    numbered_delv = []
    # if (len(delivery) % page_size == 0):
    #     extra = range(1, (len(delivery)//(page_size) + 1))
    # else:
    #     extra = range(1, (len(delivery)//(page_size)+2))
    for i, deliv in enumerate(delivery):
        numbered_delv.append((i + 1,) + deliv)
    response = {
        'admin': request.session['admin'],
        'delivery': page_split(numbered_delv, page_number - 1),
        'page': 'delivery',
        'page': page_number,
        'pages': range(1,((len(delivery)-1)//(page_size)+2))
        # 'delivery'  :   page_split(delivery, page_number -1),
        # 'sort'  : sort_method,
        # 'filter' : filter_method,
        # 'order' : order,
        # 'page' : page_number,
        # 'pages' : range(1, (len(delivery)//page_size) + 1)
    }
    return render(request, 'delivery-list.html', response)


@csrf_exempt
def delivery_add(request):
    # orderid = database.db().query('SELECT distinct A.ORDER_ID FROM APP_ORDER A, \
    #     DELIVERY D WHERE A.ORDER_ID <> D.ORDER_ID')
    address = database.db().query('SELECT * FROM ADDRESS')
    # ordered = database.db().query('SELECT * FROM ORDERED_GOODS')
    if (request.method == 'GET'):
        response = {
            'admin': request.session['admin'],
            'page': 'delivery',
            # 'orderid': orderid,
            'update': False,
            'address' : address
        }
        return render(request, 'delivery-add.html', response)
    else:
        receipt = request.POST.get('receipt_num')
        order_id = request.POST.get('order_id')
        method = request.POST.get('method')
        charge = request.POST.get('charge')
        date = request.POST.get('date')
        member_id_num = request.POST.get('member_id_num')
        member_address = request.POST.get('member_address_name')
        database.db().query('INSERT INTO DELIVERY (receipt_num, order_id, method, charge, date, member_id_num, member_address_name)  \
        VALUES (%s, %s, %s, %s, %s, %s, %s)', receipt, order_id, method, charge, date, member_id_num, member_address)
        # database.db().exec('create_delivery', receipt_num, order_id, \
        #                     method, charge, date, member_id_num, member_address)
        return redirect('delivery-list')


@csrf_exempt
def delivery_update(request):
    # deliveryup = database.db().query('SELECT * FROM DELIVERY')
    address = database.db().query('SELECT * FROM ADDRESS')
    if (request.method == 'GET'):
        receipt_num = request.GET.get('id')
        detail = database.db().query('SELECT * FROM DELIVERY WHERE receipt_num=%s',receipt_num)

        # order_id = request.

        response = {
            'admin': request.session['admin'],
            'page': 'delivery',
            'update': True,
            'detail' : detail,
            'address': address
            
        }
        return render(request, 'delivery-update.html', response)
    else:
        old_id = request.GET.get('id')
        recnum = request.POST.get('id')
        order_id = request.POST.get('order_id')
        method = request.POST.get('method')
        charge = request.POST.get('charge')
        dates = request.POST.get('date')
        member_id_num = request.POST.get('member_id_num')
        member_address = request.POST.get('member_address_name')
        database.db().query('UPDATE DELIVERY\
            SET receipt_num = %s,order_id = %s,\
                method = %s, charge = %s, date = %s,\
                    member_id_num = %s,member_address_name=%s\
                    where receipt_num = %s;',recnum,order_id,method,
                    charge,dates,member_id_num,member_address,old_id)

        return redirect('delivery-list')


def delivery_delete(request):
    receipt_num = request.GET.get('id')
    database.db().query('DELETE FROM DELIVERY WHERE receipt_num=%s', receipt_num)
    return redirect('delivery-list')


# ------------------- REVIEW

def review_list(request):
    response = {
        'admin': request.session['admin'],
        'page': 'review'
    }
    reviews = database.db().query('SELECT * FROM SENT_GOODS')
    response = {
        'admin': request.session['admin'],
        'reviews': reviews,
        'page': 're',
        'update': True
    }
    return render(request, 'review-list.html', response)


@csrf_exempt
def review_add(request):
    if (request.method == 'GET'):
        response = {
            'admin': request.session['admin'],
            'page': 'review',
            'update': False
        }
        return render(request, 'review-add.html', response)
    else:
        goods_id = request.POST.get('goodsId')
        receipt_id = request.POST.get('receiptId')
        order_num = request.POST.get('orderNum')
        review = request.POST.get('review')
        database.db().exec('create_review', goods_id, receipt_id, order_num, review)
        return redirect('review-list')


def review_delete(request):
    id = request.GET.get('id')
    ordNum = request.GET.get('ordNum')
    database.db().query('DELETE FROM SENT_GOODS WHERE id=%s AND order_num=%s', id, ordNum)
    #return redirect('level-list')
    return redirect('review-list')

@csrf_exempt
def chat_add(request):
    if (request.method == 'GET'):
        response = {
            'admin': request.session['admin'],
            'page': 'review',
            'update': False
        }
        return render(request, 'review-list.html', response)
    else:
        chat_id = request.POST.get('chatId')
        member_id = request.POST.get('memberId')
        admin_id = request.POST.get('adminId')
        message = request.POST.get('message')
        database.db().exec('add_chat', chat_id, member_id, admin_id, message)
        return redirect('chat-list')


def chat_delete(request):
    level_name = request.GET.get('removeId')
    database.db().query('DELETE FROM chat WHERE id=%s', level_name)
    #return redirect('level-list')
    return redirect('chat-list')



def page_split(collection, page_number):
    if collection is None or len(collection) == 0:
        return None
    return list(chunk(collection))[page_number]


def chunk(collection):
    for i in range(0, len(collection), page_size):
        yield collection[i:i + page_size]
