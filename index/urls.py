from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),

    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('logout/', views.logout, name='logout'),
    path('profile/', views.profile, name='profile'),

    path('mychat/', views.chat_list, name='chat-list'),
    path('mychat/reply/', views.chat_add, name='chat-reply'),
    path('mychat/delete/', views.chat_delete, name='chat-delete'),

    path('goods/', views.goods_list, name='goods-list'),
    path('goods/add/', views.goods_add, name='goods-add'),
    path('goods/update/', views.goods_update, name='goods-update'),
    path('goods/delete/', views.goods_delete, name='goods-delete'),
    path('goods/detail/', views.goods_detail, name='goods-detail'),
    path('goods/review/', views.goods_review, name='goods-review'),

    path('level/', views.level_list, name='level-list'),
    path('level/add/', views.level_add, name='level-add'),
    path('level/update/', views.level_update, name='level-update'),
    path('level/delete/', views.level_delete, name='level-delete'),

    path('order/', views.order_list, name='order-list'),
    path('order/add/', views.order_add, name='order-add'),
    path('order/update/', views.order_update, name='order-update'),
    path('order/delete/', views.order_delete, name='order-delete'),

    path('delivery/', views.delivery_list, name='delivery-list'),
    path('delivery/add/', views.delivery_add, name='delivery-add'),
    path('delivery/update/', views.delivery_update, name='delivery-update'),
    path('delivery/delete/', views.delivery_delete, name='delivery-delete'),

    path('item/', views.item_list, name='item-list'),
    path('item/add/', views.item_add, name='item-add'),
    path('item/update/', views.item_update, name='item-update'),
    path('item/delete/', views.item_delete, name='item-delete'),

    path('review/', views.review_list, name='review-list'),
    path('review/add/', views.review_add, name='review-add'),
    # path('review/update/', views.review_update, name='review-update'),
    path('review/delete/', views.review_delete, name='review-delete')
]
